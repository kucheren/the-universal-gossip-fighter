#!/usr/bin/env python
# coding: utf-8

import numpy as np
import random
from tqdm import tqdm
from pathlib import Path
from time import time

import multiprocessing
from multiprocessing import Pool

from draw import draw
from common_function import *
from constants import *

np.random.seed(seed=42)
random.seed(42)

start_time = time()

# can be indices of strategies/k values/eps values
# iter_range = [0, 1, 2, 3, 4]
iter_range = [0, 1, 2, 4]

# time complexity
t_complexity = np.empty((len(iter_range), len(n_range), repetitions))
# normalized time complexity
tau_complexity = np.empty((len(iter_range), len(n_range), repetitions))
# message complexity
m_complexity = np.empty((len(iter_range), len(n_range), repetitions))

params = []

for ind, str_ind in enumerate(iter_range):
    for n_ind, n in enumerate(n_range):
        for i in np.arange(repetitions):
            # params.append(((ind, n_ind, i), f(n), n))
            params.append(((ind, n_ind, i), n, str_ind, 'SEARS'))
            # params.append(((ind, n_ind, i), n, eps))

num_cores = multiprocessing.cpu_count()
with Pool(num_cores) as p:
    processed = p.map(exp_with_all_strategies, tqdm(params))

for (index, time, tau, messages) in tqdm(processed):
    t_complexity[index] = time
    tau_complexity[index] = tau
    m_complexity[index] = messages

# ------------------saving the results and plotting them----------------------
directory = f'../remote_pycharm/SEARS/all_strategies/{n_range[-1]}_{repetitions}'
Path(directory).mkdir(parents=True, exist_ok=True)

draw(directory, labels, 'SEARS', n_range,
     t_complexity, tau_complexity, m_complexity)

from time import time

print(f'\nExecuting time: {np.around(time() - start_time, 3)}\n')

with open(f'{directory}/cache_time.txt', 'a') as file:
    np.savetxt(file, np.array([np.around(time() - start_time, 3)]),
               header='executing time', fmt='%1.1f')
    file.close()
