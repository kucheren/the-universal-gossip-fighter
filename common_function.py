import numpy as np
import random
from copy import deepcopy
from typing import Callable

from constants import *

np.random.seed(seed=42)
random.seed(42)


class Network:
    def __init__(self, n: int, method: str):
        self.n: int = n  # number of processes
        self.method = method  # RS: SEARS/EARS/PUSH&PULL

        self.rumors = np.diag([True] * n)  # rumors[i, j] == true if i-th process knows j's rumor
        self.delay = np.full(n, 1)
        self.delta = np.full(n, 1)

        # process, rumor, another process
        self.informed_list = np.array([np.diag([True] * n) for i in range(
            n)])  # [i, j, k] is tru if i's process knows that process k knows rumor of j

        self.L = np.array([set(range(n)) - {i} for i in range(n)])  # list of future receivers (losers)
        self.is_waiting = np.full(n, 0)  # a number of global steps left before this process finishes a local step.
        # A process delivers messages during local step, but sends it only in the end.

        self.isolated_part: list = []  # control group of processes of size N-F/2
        self.isolated_vertex: int = 0  # The special chosen process (\hat{\pi})

        self.events = []

        self.strategy: Callable[[Network], int] = lambda x: 1  # adversarial attack on RS
        self.F: int = int(0.3 * n)  # total nums of crashed nodes
        self.f: int = max(1, int(np.log(n)))  # nums of crashed nodes per each step (used in trivial adversary strategy)
        self.sleep_cnt = np.full(n, get_sleep_counter(method, n, self.F))  # used in EARS, SEARS

        self.m: int = 0  # message complexity during current moment of time (inside of one run of the algorithm)
        self.t: int = 0  # time complexity during current moment of time (inside of one run of the algorithm)

        self.coin = -1
        self.DELTA = 1
        self.DELAY = 1

        self.eps = None
        self.q = None
        self.time_threshold = None
        self.counter = None

        if method == 'SEARS':
            self.init_sears()

    def init_sears(self):
        self.eps = 0.5

        # in the paper q = k * n^eps * log n
        # currently k = 1
        self.q: int = int((self.n ** self.eps) * np.log(self.n))  # nums of new messages per step

        # time after which each rumor stops to be spread
        self.time_threshold = int((2/self.eps - 1) * (self.n - self.F) / self.n)
        self.counter = np.full((self.n, self.n), self.time_threshold + 1) - np.diag([self.time_threshold + 1] * self.n)


class Event:
    def __init__(self,
                 _time: int = -1,
                 _sender: int = -1,
                 _receiver: int = -1,
                 _rumors: np.ndarray = np.empty([]),
                 _counter: np.ndarray = None,
                 _informed_list: np.ndarray = np.empty([])
                 ):
        self.time = _time  # delivery time
        self.sender = _sender
        self.receiver = _receiver
        self.rumors = _rumors
        self.informed_list = _informed_list
        self.counter = _counter  # only exists for SEARS


def get_sleep_counter(method, n, F):
    if method == 'EARS':
        return int((n / (n - F)) * np.log(n))
    elif method == 'SEARS':
        return 2
    else:
        return AssertionError('\nWrong name of strategy.\n')


def end_of_local_step(net, process: int):
    return net.t - net.t % net.delta[process] + net.delta[process]


def update_L(net: Network, process: int):
    # a raw of zeros/ones that correspond to rumors we want to spread
    spreadable_rumors = []
    if net.method == 'EARS':
        spreadable_rumors = net.rumors[process]
    elif net.method == 'SEARS':
        spreadable_rumors = net.rumors[process] * (net.counter[process] < net.time_threshold)
    else:
        AssertionError('\nWrong name of strategy.\n')
    new_L = np.arange(net.n)[spreadable_rumors @ np.logical_not(net.informed_list[process])]
    assert process not in new_L, new_L
    net.L[process] = new_L


def update_counter(net: Network):
    for process in range(net.n):
        if net.is_waiting[process]:
            continue

        net.counter[process] += 1


def sending(net: Network):
    for process in np.argwhere(net.sleep_cnt > 0).flatten():
        if net.is_waiting[process]:
            continue

        potential_receivers = list(np.delete(np.arange(net.n), process))
        _counter = None
        if net.method == 'EARS':
            receivers = [random.choice(potential_receivers)]
        elif net.method == 'SEARS':
            receivers = random.sample(potential_receivers, min(net.q, net.n - 1))
            _counter = deepcopy(net.counter[process])
        else:
            AssertionError('\nWrong name of straegy.\n')
        net.m += len(receivers)

        for receiver in receivers:
            new_event = Event(_time=end_of_local_step(net, process) + net.delay[process],
                              _sender=process,
                              _receiver=receiver,
                              _rumors=deepcopy(net.rumors[process]),
                              _counter=_counter,
                              _informed_list=deepcopy(net.informed_list[process])
                              )
            net.events.append(new_event)

            if net.method == 'EARS':
                spreadable_rumors = net.rumors[process]
            elif net.method == 'SEARS':
                spreadable_rumors = net.rumors[process] * (net.counter[process] <= net.time_threshold)
            else:
                AssertionError('\nWrong name of strategy\n')
            net.informed_list[process][:, receiver][spreadable_rumors] = True

        update_L(net, process)


def message_processing(net: Network, receiver: int,
                       rumors: np.ndarray,
                       informed_list: np.ndarray,
                       counter: np.ndarray = np.empty([])):
    if net.sleep_cnt[receiver] < 0:
        return

    net.rumors[receiver][rumors] = True
    if net.method == 'SEARS':
        net.counter[receiver] = np.minimum(net.counter[receiver], counter)

    net.informed_list[receiver] = np.logical_or(net.informed_list[receiver],
                                                informed_list)
    net.informed_list[receiver][:, receiver] = net.rumors[receiver]

    update_L(net, receiver)


def events_processing(net: Network):
    net.events = sorted(net.events, key=lambda x: x.time)

    assert len(net.events) == 0 or net.events[0].time >= net.t, 'Wrong events order'
    while net.events and net.events[0].time == net.t:
        event = net.events[0]

        message_processing(net, event.receiver, event.rumors,
                           event.informed_list, event.counter)

        net.events = net.events[1:]


def communication_is_over(net: Network) -> bool:
    return not (np.any(net.sleep_cnt > 0) or net.events)


def strategy_0(net: Network):
    if not net.t:
        net.sleep_cnt[net.isolated_part] = -1


def strategy_1(net: Network):
    if not net.t:
        net.delay[net.isolated_part] = net.DELAY
        net.delta[net.isolated_part] = net.DELTA


def strategy_2(net: Network):
    if not net.t:
        net.sleep_cnt[net.isolated_part] = -1
        net.sleep_cnt[net.isolated_vertex] = get_sleep_counter(net.method,
                                                               net.n, net.F)
        net.delta[net.isolated_vertex] = net.DELTA

    for event in net.events:
        sender = event.sender
        receiver = event.receiver

        if sender != net.isolated_vertex:
            continue

        if len(np.argwhere(net.sleep_cnt < 0)) < net.F:
            net.sleep_cnt[receiver] = -1


def strategy_3(net: Network):
    alive = np.argwhere(net.sleep_cnt > 0).flatten()
    len_alive = len(alive)
    len_crashed = len(np.argwhere(net.sleep_cnt < 0).flatten())

    n_crashed = min(net.F - len_crashed, net.f, len_alive)
    if not n_crashed:
        return

    # # amount of information (type 1: rumors)
    # most_informed = np.argsort([len(i) for i in net.rumors[alive]])[::-1]

    # amount of information (type 2: informed list)
    most_informed = np.argsort([np.sum(i) for i in net.informed_list[alive]])[::-1]
    # last brace corresponds to inverting the list (to make it in decreasing order)

    for process in most_informed[:n_crashed]:
        net.sleep_cnt[process] = -1


def strategy_4(net: Network):
    pass


def adversary(net: Network):
    net.isolated_part = random.sample(range(net.n), int(0.5 * net.F + 1))
    net.isolated_process = random.sample(net.isolated_part, 1)


def double_check(net: Network):
    if net.t is None:
        return

    if net.method == 'SEARS':
        return

    is_crashed = (net.sleep_cnt < 0)
    is_sleeping = (net.sleep_cnt == 0)

    for process in np.argwhere(is_sleeping):
        known_rumors = np.logical_or(net.rumors[process], is_crashed)

        if net.sleep_cnt[process] == 0 and not np.all(known_rumors):
            missing_rumors = np.logical_not(known_rumors)
            print()
            print('\nTermination condition error')
            print(net.strategy)
            print(f'amount of vertices = {net.n}')
            print(f'failed process = {process}')
            print(f'missing rumors:\n{np.arange(net.n)[missing_rumors]}')
            print(f'crashed:\n{np.arange(net.n)[is_crashed]}')


def imitate(net: Network) -> Network:
    adversary(net)

    while True:
        net.strategy(net)  # adversarial attack

        events_processing(net)

        if net.method == 'SEARS':
            # increase counter
            update_counter(net)
        elif net.method == 'EARS':
            pass
        else:
            AssertionError('\nWrong name of strategy\n')

        for process in range(net.n):
            if net.is_waiting[process]:
                continue

            if net.sleep_cnt[process] < 0:
                continue

            if len(net.L[process]) == 0:
                net.sleep_cnt[process] = max(0, net.sleep_cnt[process] - 1)
            else:
                if net.method == 'EARS':
                    net.sleep_cnt[process] = get_sleep_counter(net.method, net.n, net.F)
                elif net.method == 'SEARS':
                    # if sleep_counter == 0 then do not wake up
                    # e.g. do not update sleep_counter
                    if net.sleep_cnt[process] > 0:
                        net.sleep_cnt[process] = get_sleep_counter(net.method, net.n, net.F)
                else:
                    ValueError(f'\nWrong type of model: {net.method}\n')


        sending(net)

        for process in np.argwhere(net.sleep_cnt > 0).flatten():
            if net.is_waiting[process]:
                net.is_waiting[process] -= 1
            else:
                net.is_waiting[process] = net.delta[process] - 1

        net.t += 1  # global time

        if communication_is_over(net) or net.t > TIME_THRESHOLD:  # magic const
            if net.t > TIME_THRESHOLD:
                net.t = None
                net.m = None
                print('\nTerminated only', len(np.argwhere(net.sleep_cnt <= 0)) / net.n * 100,
                      '% of all vertices\n')
            break

    return net


def exp_with_all_strategies(args):
    index, n, str_ind, method = args
    net = Network(n, method)

    delta_match = [1, net.F, net.F, 1, 1]
    delay_match = [1, net.F ** 2, 1, 1, 1]

    net.DELTA = delta_match[str_ind]
    net.DELAY = delay_match[str_ind]
    net.strategy = strategies[str_ind]

    resulting_network = imitate(net)
    double_check(resulting_network)

    time = resulting_network.t
    factor = max(1, resulting_network.DELTA + resulting_network.DELAY)
    normalized_time = resulting_network.t / factor if resulting_network.t is not None else None
    messages = resulting_network.m

    return (index, time, normalized_time, messages)


def exp_with_second_strategy(args):
    index, n, eps, method = args
    net = Network(n, method)

    net.eps = eps
    net.q = int((n ** eps) * np.log(n))
    net.time_threshold = int(net.n / ((net.n - net.F) * eps))
    net.DELTA = net.F
    net.DELAY = 1
    net.strategy = strategies[2]

    res_net = imitate(net)
    double_check(res_net)

    time = res_net.t
    factor = max(1, res_net.DELTA + res_net.DELAY)
    normalized_time = res_net.t / factor if res_net.t else None
    messages = res_net.m

    return (index, time, normalized_time, messages)


def exp_with_third_strategy(args):
    index, n, step_ind, method = args
    net = Network(n, method)

    net.strategy = strategies[3]
    net.f = n_craches_per_step[step_ind](n)

    res_net = imitate(net)
    double_check(res_net)

    time = res_net.t
    factor = max(1, res_net.DELTA + res_net.DELAY)
    normalized_time = res_net.t / factor if res_net.t else None
    messages = res_net.m

    return (index, time, normalized_time, messages)


strategies = {
    0: lambda x: strategy_0(x),
    1: lambda x: strategy_1(x),
    2: lambda x: strategy_2(x),
    3: lambda x: strategy_3(x),
    4: lambda x: strategy_4(x)
}
