import numpy as np

TIME_THRESHOLD = 10 ** 10
#n_range = np.array([10, 40, 80, 160, 320, 640, 1000])
#n_range = np.array([10, 20])
# n_range = np.linspace(10, 310, 16, dtype=int)
# n_range = np.linspace(10, 50, 5, dtype=int)
n_range = np.array([10, 20, 30, 50, 70, 100, 200, 300, 400, 500])

# eps_range = np.array([0.05, 0.1, 0.2, 0.3, 0.5, 1])
repetitions = 100

labels = ['Crash C',
          'Delay C',
          'Crash all C but one',
          'Greedy adversary',
          'No adversary']

