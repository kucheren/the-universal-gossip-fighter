# The Universal Gossip Fighter

The framework that evaluates the impact of the Universal Gossip Fighter on the time and message complexities of several gossip protocols in various system configurations.
