import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm
import datetime
import seaborn as sns
from constants import *


def draw(directory, labels, strategy_name, n_range,
         t_complexity, tau_complexity, m_complexity):
    sns.set_style('whitegrid')

    pic1, ax1 = plt.subplots(1, 1, figsize=(13, 10))
    pic2, ax2 = plt.subplots(1, 1, figsize=(13, 10))

    y_min = np.nanquantile(tau_complexity[-1], 0.25, axis=1)
    y_max = np.nanquantile(tau_complexity[-1], 0.75, axis=1)
    ax1.plot(n_range, y_max, color=f'C4',ls='dotted', lw=2)
    ax1.plot(n_range, np.nanmedian(tau_complexity[-1], axis=1), label='No adversary', color=f'C4', lw=5, linestyle='-.')
    ax1.plot(n_range, y_min, color=f'C4',ls='dotted', lw=2)
    ax1.fill_between(n_range, y_min, y_max, alpha=0.07, color=f'C4')

    y_min = np.nanquantile(m_complexity[-1], 0.25, axis=1)
    y_max = np.nanquantile(m_complexity[-1], 0.75, axis=1)
    ax2.plot(n_range, y_max, color=f'C4', ls='dotted', lw=2)
    ax2.plot(n_range, np.nanmedian(m_complexity[-1], axis=1), label='No adversary', color=f'C4', lw=5, linestyle='-.')
    ax2.plot(n_range, y_min, color=f'C4', ls='dotted', lw=2)
    ax2.fill_between(n_range, y_min, y_max, alpha=0.07, color=f'C4')

    #-------------------------------------------------------------------------#
    # TIME

    normalized_mean_time = np.nanmean(tau_complexity[[0, 1, 2]], axis=0)
    assert normalized_mean_time.shape == (len(n_range), repetitions)
    y_min = np.nanquantile(normalized_mean_time, 0.25, axis=1)
    y_max = np.nanquantile(normalized_mean_time, 0.75, axis=1)
    ax1.plot(n_range, y_max, color=f'C0', ls='dotted', lw=2)
    ax1.plot(n_range, np.nanmedian(normalized_mean_time, axis=1), label='UGF', color=f'C0', lw=5, linestyle='-')
    ax1.plot(n_range, y_min, color=f'C0', ls='dotted', lw=2)
    ax1.fill_between(n_range, y_min, y_max, alpha=0.07, color=f'C0')
   
    # max strategy # ----------------------------------------------------------
    tmp_str=0
    if strategy_name == 'EARS':
        tmp_str=2
    
    y_min = np.nanquantile(tau_complexity[tmp_str], 0.25, axis=1)
    y_max = np.nanquantile(tau_complexity[tmp_str], 0.75, axis=1)
    ax1.plot(n_range, y_max, color=f'C1', ls='dotted', lw=2)
    ax1.plot(n_range, np.nanmedian(tau_complexity[tmp_str], axis=1), label='Max UGF strategy', color=f'C1', lw=5, linestyle='--')
    ax1.plot(n_range, y_min, color=f'C1', ls='dotted', lw=2)
    ax1.fill_between(n_range, y_min, y_max, alpha=0.07, color=f'C1')

    # -----------------------------------------
    # labels
    ax1.set_xlabel('Number of processes', fontsize=30)
    ax1.set_ylabel('Time complexity', fontsize=30)
    ax1.set_title(f'Normalised time complexity [{strategy_name}]', fontsize=30, pad=20)
    ax1.tick_params(labelsize=30)
    pic1.tight_layout()
    pic1.savefig(f'{directory}/graph_time_{datetime.datetime.now()}.png', bbox_inches="tight")

    # -------------------------------------------------------------
    # MESSAGE

    # mean strategy
    normalized_mean_message = np.nanmean(m_complexity[[0, 1, 2]], axis=0)
    assert normalized_mean_message.shape == (len(n_range), repetitions)
    y_min = np.nanquantile(normalized_mean_message, 0.25, axis=1)
    y_max = np.nanquantile(normalized_mean_message, 0.75, axis=1)
    ax2.plot(n_range, y_max, color=f'C0', ls='dotted', lw=2)
    ax2.plot(n_range, np.nanmedian(normalized_mean_message, axis=1), label='UGF', color=f'C0', lw=5, linestyle='-')
    ax2.plot(n_range, y_min, color=f'C0', ls='dotted', lw=2)
    ax2.fill_between(n_range, y_min, y_max, alpha=0.07, color=f'C0')

   
    # max strategy
    y_min = np.nanquantile(m_complexity[1], 0.25, axis=1)
    y_max = np.nanquantile(m_complexity[1], 0.75, axis=1)
    ax2.plot(n_range, y_max, color=f'C1', ls='dotted', lw=2)
    ax2.plot(n_range, np.nanmedian(m_complexity[1], axis=1), label='Max UGF strategy', color=f'C1', lw=5, linestyle='--')
    ax2.plot(n_range, y_min, color=f'C1', ls='dotted', lw=2)
    ax2.fill_between(n_range, y_min, y_max, alpha=0.07, color=f'C1')

    # labels
    ax2.set_xlabel('Number of processes', fontsize=30)
    ax2.set_ylabel('Message complexity', fontsize=30)
    ax2.set_title(f'Message complexity [{strategy_name}]', fontsize=30, pad=20)
    ax2.tick_params(labelsize=30)
    pic2.tight_layout()
    pic2.savefig(f'{directory}/graph_message_{datetime.datetime.now()}.png')
