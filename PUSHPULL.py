#!/usr/bin/env python
# coding: utf-8

import numpy as np
import random
from typing import List, Dict, Set, Tuple, Callable
from tqdm import tqdm
import matplotlib.pyplot as plt
import seaborn as sns
from copy import deepcopy
import itertools
from pathlib import Path
import multiprocessing
from multiprocessing import Pool

sns.set_style('darkgrid')

np.random.seed(seed=42)
random.seed(42)
from time import time

from draw import draw
from common_function import *
from constants import *

start_time = time()


class Network:
    def __init__(self, n: int):
        self.n: int = n

        self.rumors = np.diag([True] * n)
        self.delay = np.full(n, 1)
        self.delta = np.full(n, 1)

        self.status = np.full(n, 1)
        self.is_not_pushed = np.logical_not(np.diag([True] * n))
        self.is_not_pulled = np.logical_not(np.diag([True] * n))
        # self.is_pushed = np.diag([True] * n)
        # self.is_pulled = np.diag([True] * n)

        self.is_waiting = np.full(n, 0)

        self.isolated_part: list = []
        self.isolated_vertex: int = 0

        self.events = []

        self.strategy: Callable[[Network], int] = lambda x: 1  # nums of crashed processes per step
        self.F: int = int(0.3 * n)  # total nums of crashed nodes
        # equal to F
        self.f: int = int(0.1 * n)  # nums of crashed nodes per each step

        self.m: int = 0  # message complexity during current round
        self.t: int = 0  # time complexity during current round

        self.coin = -1
        self.DELTA = 1
        self.DELAY = 1


class Event:
    def __init__(self, time: int = -1, sender: int = -1, receiver: int = -1,
                 rumors: np.ndarray = np.empty([]), is_push: bool = True):
        self.time = time
        self.sender = sender
        self.receiver = receiver
        self.rumors = rumors
        self.is_push = is_push


def end_of_local_step(net: Network, process: int):
    return net.t - net.t % net.delta[process] + net.delta[process]


def sending(net: Network):
    # TODO: check type of message [could be changed to some structure]
    for process in np.argwhere(net.is_waiting == 0).flatten():
        if net.status[process] != 1:
            # non active process can't send messages
            continue

        known_rumors = np.logical_or(net.rumors[process],
                                     np.logical_not(net.is_not_pulled[process]))

        if np.all(known_rumors):
            # termination condition is executed
            net.status[process] = 0
            continue

        # push stage
        if np.any(net.is_not_pushed[process]):
            receiver = random.choice(np.arange(net.n)[net.is_not_pushed[process]])
            net.m += 1

            net.is_not_pushed[process][receiver] = False
            new_event = Event(time=end_of_local_step(net, process) + net.delay[process],
                              sender=process,
                              receiver=receiver,
                              rumors=deepcopy(net.rumors[process])
                              )
            net.events.append(new_event)

        # pull stage
        missing_rumors = np.logical_not(known_rumors)
        receiver = random.choice(np.arange(net.n)[missing_rumors])
        net.m += 1

        net.is_not_pulled[process][receiver] = False
        new_event = Event(time=end_of_local_step(net, process) + net.delay[process],
                          sender=process,
                          receiver=receiver,
                          is_push=False
                          )
        net.events.append(new_event)


def message_processing(net: Network, receiver: int, rumors: np.ndarray):
    if net.status[receiver] == -1:
        # receiver was crashed
        return

    net.rumors[receiver][rumors] = True


def request_processing(net: Network, sender: int, receiver: int):
    if net.status[receiver] != -1:
        # receiver was not crashed
        net.m += 1
        new_event = Event(time=end_of_local_step(net, receiver) + net.delay[receiver],
                          sender=receiver,
                          receiver=sender,
                          rumors=deepcopy(net.rumors[receiver])
                          )
        net.events.append(new_event)


def events_processing(net: Network):
    net.events = sorted(net.events, key=lambda x: x.time)

    assert len(net.events) == 0 or net.events[0].time >= net.t, 'Wrong events order'
    while net.events and net.events[0].time == net.t:
        message: Event = net.events[0]

        if message.is_push:
            message_processing(net, message.receiver, message.rumors)
        else:
            request_processing(net, message.sender, message.receiver)

        net.events = net.events[1:]


def communication_is_over(net: Network) -> bool:
    return np.all(net.status <= 0) and not net.events


def strategy_0(net: Network):
    if not net.t:
        net.status[net.isolated_part] = -1

#def strategy_0(net: Network):
#    net.isolated_part1 = random.sample(range(net.n), int(net.F + 1))
#    if not net.t:
#        net.status[net.isolated_part1] = -1



def strategy_1(net: Network):
    if not net.t:
        net.delay[net.isolated_part] = net.DELAY
        net.delta[net.isolated_part] = net.DELTA


def strategy_2(net: Network):
    if not net.t:
        net.status[net.isolated_part] = -1
        net.status[net.isolated_vertex] = 1
        net.delta[net.isolated_vertex] = net.DELTA

    for event in net.events:
        sender = event.sender
        receiver = event.receiver

        if sender != net.isolated_vertex:
            continue

        if np.sum(net.status < 0) < net.F:
            net.status[receiver] = -1


def strategy_3(net: Network):
    alive = np.argwhere(net.status > 0).flatten()
    len_alive = len(alive)
    len_crashed = np.sum(net.status == -1)

    #n_crashed = min(net.F - len_crashed, net.f, len_alive)  # magic const
    n_crashed = min(net.F - len_crashed, net.f, len_alive)  # magic const
    
    if not n_crashed:
        return

    # amount of information (type 1: rumors)
    most_informed = np.argsort([np.sum(i) for i in net.rumors[alive]])[::-1]

    for process in most_informed[:n_crashed]:
        net.status[process] = -1


def strategy_4(net: Network):
    pass


def adversary(net: Network):
    net.isolated_part = random.sample(range(net.n), int(0.5 * net.F + 1))
    net.isolated_process = random.sample(net.isolated_part, 1)


def double_check(net: Network):
    if net.t is None:
        return

    is_crashed = (net.status < 0)
    is_sleeping = (net.status == 0)

    for process in np.argwhere(is_sleeping):
        known_rumors = np.logical_or(net.rumors[process], is_crashed)

        if net.status[process] == 0 and not np.all(known_rumors):
            missing_rumors = np.logical_not(known_rumors)
            print('\nTermination condition error')
            print(f'failed process = {process}')
            print(f'missing rumors:\n{np.arange(net.n)[missing_rumors]}')
            print(f'crashed:\n{np.arange(net.n)[is_crashed]}')
            break


def imitate(net) -> Network:
    adversary(net)

    while True:
        net.strategy(net)

        sending(net)

        for process in np.argwhere(net.status == 1).flatten():
            if net.is_waiting[process]:
                net.is_waiting[process] -= 1
            else:
                net.is_waiting[process] = net.delta[process] - 1

        events_processing(net)

        net.t += 1

        if communication_is_over(net) or net.t > TIME_THRESHOLD:  # magic const
            if net.t > TIME_THRESHOLD:
                net.t = None
                net.m = None
                print('\nTerminated only', len(np.argwhere(net.status <= 0)) / net.n * 100,
                      '% of all vertices\n')
            break

    return net


def exp_with_all_strategies(args):
    index, n, str_ind = args
    net = Network(n)

    delta_match = [1, net.F, net.F, 1, 1]
    delay_match = [1, net.F ** 2, 1, 1, 1]

    net.DELTA = delta_match[str_ind]
    net.DELAY = delay_match[str_ind]
    net.strategy = strategies[str_ind]

    res_net = imitate(net)
    double_check(res_net)

    time = res_net.t
    factor = max(1, res_net.DELTA + res_net.DELAY)
    normalized_time = res_net.t / factor if res_net.t else None
    messages = res_net.m

    return (index, time, normalized_time, messages)


def exp_with_third_strategy(args):
    index, n, step = args
    net = Network(n)

    net.strategy = strategies[3]
    net.f = step

    res_net = imitate(net)
    double_check(res_net)

    time = res_net.t
    factor = max(1, res_net.DELTA + res_net.DELAY)
    normalized_time = res_net.t / factor if res_net.t is not None else None
    messages = res_net.m

    return (index, time, normalized_time, messages)


strategies = {
    0: lambda x: strategy_0(x),
    1: lambda x: strategy_1(x),
    2: lambda x: strategy_2(x),
    3: lambda x: strategy_3(x),
    4: lambda x: strategy_4(x)
}

# n_craches_per_step = [
#     lambda x: 0,
#     lambda x: 1,
#     # lambda x: 2,
#     # lambda x: 5,
#     # lambda x: int(3 * np.log(x)),
#     # lambda x: int(0.25 * np.sqrt(x)),
#     # lambda x: int(0.025 * x),
#     lambda x: int(0.1 * x),
#     lambda x: int(0.5 * x)
# ]

t_complexity = np.empty((len(strategies), len(n_range), repetitions))
tau_complexity = np.empty((len(strategies), len(n_range), repetitions))
m_complexity = np.empty((len(strategies), len(n_range), repetitions))

params = []

# for ind, per_step in enumerate(n_craches_per_step):
for ind, (str_ind, strategy) in enumerate(strategies.items()):
    for n_ind, n in enumerate(n_range):
        for i in np.arange(repetitions):
            # params.append(((ind, n_ind, i), n, per_step(n)))
            params.append(((ind, n_ind, i), n, str_ind))

num_cores = multiprocessing.cpu_count()
with Pool(num_cores) as p:
    processed = p.map(exp_with_all_strategies, tqdm(params))

for (index, time, tau, messages) in tqdm(processed):
    t_complexity[index] = time
    tau_complexity[index] = tau
    m_complexity[index] = messages

# ------------------saving the results and plotting them----------------------
directory = f'../remote_pycharm/PUSH&PULL/all_strategies/{n_range[-1]}_{repetitions}'
Path(directory).mkdir(parents=True, exist_ok=True)

draw(directory, labels, 'Push-Pull', n_range,
     t_complexity, tau_complexity, m_complexity)

from time import time

print(f'\nExecuting time: {np.around(time() - start_time, 3)}\n')

with open(f'{directory}/cache_time.txt', 'a') as file:
    np.savetxt(file, np.array([np.around(time() - start_time, 3)]),
               header='execute time', fmt='%1.1f')
    file.close()
